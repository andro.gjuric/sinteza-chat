// require the express module
const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');
const http = require('http');
const io = require('socket.io');
const path = require('path');

const mongodburl = 'mongodb+srv://chat:XUBP5sZsSOxyRlxx@cluster0-t4vns.mongodb.net/chat?retryWrites=true&w=majority';

class ChatApp
{
    constructor()
    {
        const me = this;

        me.db = null;
        me.chats = {};
        me.adminSockets = [];

        (async () =>
        {
            const client = await MongoClient.connect(mongodburl, {
                useUnifiedTopology: true,
            });
            me.db = client.db('chat');
            await me.initApp();
        })();
    }

    initApp()
    {
        const me = this;
        const app = express();
        // require the http module
        const httpserv = http.Server(app);
        // integrating socketio
        const socketIo = io(httpserv);
        const port = process.env.PORT || 8000;

        // bodyparser middleware
        app.use(bodyParser.json());

        app.use((req, res, next) =>
        {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            next();
        });

        // express
        app.use(express.static(`${__dirname}/public`));
        app.use('/admin', express.static(path.join(__dirname, 'admin')));
        app.use('/chat', express.static(path.join(__dirname, 'chat')));

        app.get('/admin', async (req, res) =>
        {
            res.sendFile(`${__dirname}/admin/index.html`);
        });

        app.get('/chat', async (req, res) =>
        {
            res.sendFile(`${__dirname}/chat/index.html`);
        });

        app.get('/fetchadminchats', async (req, res) =>
        {
            try
            {
                const result = await me.db.collection('chats').find({}).toArray();
                res.json(result);
            }
            catch (error)
            {
                console.error(error);
                res.send('error');
            }
        });

        app.get('/fetchuserchat', async (req, res) =>
        {
            try
            {
                const result = await me.db.collection('chats').findOne({
                    socketId: req.query.socketId,
                });
                res.json(result);
            }
            catch (error)
            {
                console.error(error);
                res.send('error');
            }
        });

        app.post('/deletechat', async (req, res) =>
        {
            try
            {
                await me.db.collection('chats').deleteOne({
                    socketId: req.body.socketId,
                });

                if (me.chats[req.body.socketId] !== undefined)
                {
                    me.chats[req.body.socketId].emit('endchat', '');
                }

                delete (me.chats[req.body.socketId]);

                res.send('ok');
            }
            catch (error)
            {
                console.error(error);
                res.send('error');
            }
        });

        app.post('/markAsDone', async (req, res) =>
        {
            try
            {
                await me.db.collection('chats').updateOne({
                    socketId: req.body.socketId,
                },
                {
                    $set: {
                        status: 'done',
                    },
                });

                if (me.chats[req.body.socketId] !== undefined)
                {
                    me.chats[req.body.socketId].emit('endchat', '');
                }

                delete (me.chats[req.body.socketId]);

                res.send('ok');
            }
            catch (error)
            {
                console.error(error);
                res.send('error');
            }
        });

        // setup event listener
        socketIo.on('connection', (socket) =>
        {
            console.log('user connected');

            socket.on('disconnect', () =>
            {
                console.log('user disconnected');
            });

            socket.on('admin', async () =>
            {
                try
                {
                    console.log('admin connected');

                    if (me.adminSockets[socket.id] === undefined)
                    {
                        me.adminSockets[socket.id] = socket;
                        console.log(`admins socket ${socket.id}`);
                    }
                }
                catch (err)
                {
                    console.error(err);
                }
            });

            socket.on('message', async (msg) =>
            {
                try
                {
                    console.log(`message: ${msg}`);

                    console.log(`sender id ${socket.id}`);

                    if (me.chats[msg.socketId] === undefined)
                    {
                        me.chats[msg.socketId] = socket;
                        await me.db.collection('chats').insertOne({
                            socketId: msg.socketId,
                            status: 'active',
                            messages: [],
                        });
                    }

                    msg.status = 'active';

                    const message = msg;

                    await me.db.collection('chats').updateOne({
                        socketId: msg.socketId,
                    }, {
                        $push: {
                            messages: message,
                        },
                    });

                    for (const socketId in me.adminSockets)
                    {
                        if (socketId !== undefined)
                        {
                            me.adminSockets[socketId].emit('received', message);
                        }
                    }
                }
                catch (err)
                {
                    console.error(err);
                }
            });

            socket.on('adminmessage', async (message) =>
            {
                if (me.chats[message.socketId] !== undefined)
                {
                    await me.db.collection('chats').updateOne({
                        socketId: message.socketId,
                    }, {
                        $push: {
                            messages: message,
                        },
                    });

                    me.chats[message.socketId].emit('adminreply', message);
                }
            });
        });

        httpserv.listen(port, () =>
        {
            console.log(`Running on Port: ${port}`);
        });
    }
}

// eslint-disable-next-line no-new
new ChatApp();
